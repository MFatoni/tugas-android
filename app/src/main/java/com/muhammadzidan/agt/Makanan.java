package com.muhammadzidan.agt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class Makanan extends AppCompatActivity {

    DBHelper helper;
    Spinner jenis;
    EditText jumlah;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_makanan);
        jenis = (Spinner)findViewById(R.id.jenis);
        jumlah = (EditText)findViewById(R.id.jumlah);
        helper = new DBHelper(this);
    }
    public void btn_click(View view){
        ContentValues values = new ContentValues();
        values.put(DBHelper.row_jumlah, Integer.parseInt(jumlah.getText().toString()));
        values.put(DBHelper.row_jenis, jenis.getSelectedItem().toString());
        try {
            Toast.makeText(Makanan.this, "Sukses ubah data", Toast.LENGTH_SHORT).show();
            helper.ubahBahanMakanan(values);
        }catch (SQLiteException e){
            Toast.makeText(Makanan.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}