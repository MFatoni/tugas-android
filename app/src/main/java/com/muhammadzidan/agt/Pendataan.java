package com.muhammadzidan.agt;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Pendataan extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView listView;
    DBHelper helper;
    LayoutInflater inflater;
    View dialogView;
    TextView nama, jumlah, jenis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pendataan);

        Button bTambahBahan = (Button) findViewById(R.id.tambah_bahan);
        bTambahBahan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tambahBahan();
            }
        });

        helper = new DBHelper(this);
        listView = (ListView)findViewById(R.id.list_data);
        listView.setOnItemClickListener(this);
    }
    public void tambahBahan(){
        String jenis = getIntent().getExtras().getString("jenis");
        Intent intent = new Intent(this, TambahBahan.class);
        intent.putExtra("jenis",jenis);
        startActivity(intent);
    }
    public void setListView(){
        String data = getIntent().getExtras().getString("jenis");
        Cursor cursor = helper.bahan(data);
        CustomCursorAdapter customCursorAdapter = new CustomCursorAdapter(this, cursor, 1);
        listView.setAdapter(customCursorAdapter);
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int i, long x) {
        TextView getId = (TextView)view.findViewById(R.id.listID);
        final long id = Long.parseLong(getId.getText().toString());
        final Cursor cur = helper.DetailBahan(id);
        cur.moveToFirst();

        final AlertDialog.Builder builder = new AlertDialog.Builder(Pendataan.this);
        builder.setTitle("Pilih Opsi");

        //Add a list
        String[] options = {"Lihat Data", "Edit Data", "Hapus Data"};
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case 0:
                        final AlertDialog.Builder viewData = new AlertDialog.Builder(Pendataan.this);
                        inflater = getLayoutInflater();
                        dialogView = inflater.inflate(R.layout.activity_detail_bahan, null);
                        viewData.setView(dialogView);
                        viewData.setTitle("Lihat Data");

                        nama = (TextView)dialogView.findViewById(R.id.nama);
                        jumlah = (TextView)dialogView.findViewById(R.id.jumlah);
                        jenis = (TextView)dialogView.findViewById(R.id.jenis);

                        nama.setText("Nama bahan:" + cur.getString(cur.getColumnIndex(DBHelper.row_nama)));
                        jumlah.setText("Jumlah :" + cur.getString(cur.getColumnIndex(DBHelper.row_jumlah)));
                        jenis.setText("Jenis bahan : " + cur.getString(cur.getColumnIndex(DBHelper.row_jenis)));

                        viewData.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        viewData.show();
                }
                switch (which){
                    case 1:
                        Intent iddata = new Intent(Pendataan.this, UbahBahan.class);
                        iddata.putExtra(DBHelper.row_id, id);
                        startActivity(iddata);
                }
                switch (which){
                    case 2:
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(Pendataan.this);
                        builder1.setMessage("Data ini akan dihapus.");
                        builder1.setCancelable(true);
                        builder1.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                helper.hapusBahan(id);
                                Toast.makeText(Pendataan.this, "Data Terhapus", Toast.LENGTH_SHORT).show();
                                setListView();
                            }
                        });
                        builder1.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog alertDialog = builder1.create();
                        alertDialog.show();
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setListView();
    }
}