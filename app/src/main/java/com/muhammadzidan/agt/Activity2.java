package com.muhammadzidan.agt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        ImageButton ibTambahan = (ImageButton) findViewById(R.id.b_tambahan);
        ImageButton ibPokok = (ImageButton) findViewById(R.id.b_pokok);
        ImageButton ibPelengkap = (ImageButton) findViewById(R.id.b_pelengkap);
        ImageButton ibLain = (ImageButton) findViewById(R.id.b_lain);
        ibTambahan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tambahan();
            }
        });
        ibPokok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pokok();
            }
        });
        ibPelengkap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pelengkap();
            }
        });
        ibLain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lain();
            }
        });
    }
    public void tambahan(){
        Intent intent = new Intent(this, Pendataan.class);
        intent.putExtra("jenis","Bahan tambahan");
        startActivity(intent);
    }
    public void pokok(){
        Intent intent = new Intent(this, Pendataan.class);
        intent.putExtra("jenis","Bahan pokok");
        startActivity(intent);
    }
    public void pelengkap(){
        Intent intent = new Intent(this, Pendataan.class);
        intent.putExtra("jenis","Bahan pelengkap");
        startActivity(intent);
    }
    public void lain(){
        Intent intent = new Intent(this, Pendataan.class);
        intent.putExtra("jenis","Bahan lain");
        startActivity(intent);
    }
}