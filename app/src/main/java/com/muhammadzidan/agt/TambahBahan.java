package com.muhammadzidan.agt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class TambahBahan extends AppCompatActivity {

    DBHelper helper;
    EditText nama,jumlah;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_bahan);
        nama = (EditText)findViewById(R.id.nama);
        jumlah = (EditText)findViewById(R.id.jumlah);
        helper = new DBHelper(this);
    }
    public void btn_click(View view){
        String jenis = getIntent().getExtras().getString("jenis");
        ContentValues values = new ContentValues();
        values.put(DBHelper.row_nama, nama.getText().toString());
        values.put(DBHelper.row_jumlah, Integer.parseInt(jumlah.getText().toString()));
        values.put(DBHelper.row_jenis, jenis);
        try {
            Toast.makeText( TambahBahan.this, "Sukses tambah data", Toast.LENGTH_SHORT).show();
            helper.tambahBahan(values);
        }catch (SQLiteException e){
            Toast.makeText(TambahBahan.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

}