package com.muhammadzidan.agt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton ibPendataan = (ImageButton) findViewById(R.id.pendataan);
        ibPendataan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pendataan();
            }
        });


        ImageButton ibMakanan = (ImageButton) findViewById(R.id.barang_keluar);
        ibMakanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makanan();
            }
        });
    }
    public void pendataan(){
        Intent intent = new Intent(this, Activity2.class);
        startActivity(intent);
    }
    public void makanan(){
        Intent intent = new Intent(this, Makanan.class);
        startActivity(intent);
    }
}