package com.muhammadzidan.agt;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    public static final String database_name = "db_bahan";
    public static final String table_name = "tabel_bahan";

    public static final String row_id = "_id";
    public static final String row_nama = "nama";
    public static final String row_jumlah = "jumlah";
    public static final String row_jenis = "jenis";

    private SQLiteDatabase db;

    private Resources context;

    public DBHelper(Context context) {
        super(context, database_name, null, 2);
        this.context = context.getResources();
        db = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + table_name + "(" + row_id + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + row_nama + " TEXT, " + row_jumlah + " INTEGER, " + row_jenis + " TEXT)";
        db.execSQL(query);

        String[] bahanPokok = this.context.getStringArray(R.array.bahan_pokok);
        String[] bahanTambahan = this.context.getStringArray(R.array.bahan_tambahan);
        String[] bahanPelengkap = this.context.getStringArray(R.array.bahan_pelengkap);
        String[] bahanLain = this.context.getStringArray(R.array.bahan_lain);
        for(int i = 0; i< bahanPokok.length; i++){
            String sql = "INSERT INTO " + table_name + "(nama,jumlah,jenis) values ('"+bahanPokok[i]+"',0,'Bahan pokok');";
            db.execSQL(sql);
        }
        for(int i = 0; i< bahanTambahan.length; i++){
            String sql = "INSERT INTO " + table_name + "(nama,jumlah,jenis) values ('"+bahanTambahan[i]+"',0,'Bahan tambahan');";
            db.execSQL(sql);
        }
        for(int i = 0; i< bahanPelengkap.length; i++){
            String sql = "INSERT INTO " + table_name + "(nama,jumlah,jenis) values ('"+bahanPelengkap[i]+"',0,'Bahan pelengkap');";
            db.execSQL(sql);
        }
        for(int i = 0; i< bahanLain.length; i++){
            String sql = "INSERT INTO " + table_name + "(nama,jumlah,jenis) values ('"+bahanLain[i]+"',0,'Bahan lain');";
            db.execSQL(sql);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int x) {
        db.execSQL("DROP TABLE IF EXISTS " + table_name);
    }

    public Cursor bahan(String jenis){
        Cursor cur = db.rawQuery("SELECT * FROM " + table_name + " WHERE " + row_jenis + "=" + "'"+jenis+"';", null);
        return cur;
    }

    //Get All SQLite Data
    public Cursor semuaBahan(){
        Cursor cur = db.rawQuery("SELECT * FROM " + table_name, null);
        return cur;
    }

    //Get 1 Data By ID
    public Cursor DetailBahan(Long id){
        Cursor cur = db.rawQuery("SELECT * FROM " + table_name + " WHERE " + row_id + "=" + id, null);
        return cur;
    }

    //Insert Data to Database
    public void tambahBahan(ContentValues values){
        db.insert(table_name, null, values);
    }

    //Update Data
    public void ubahBahan(ContentValues values, long id){
        db.update(table_name, values, row_id + "=" + id, null);
    }
    public void ubahBahanMakanan(ContentValues values){
        Integer jumlah = (Integer) values.get("jumlah");
        String jenis = (String) values.get("jenis");
        String sql = "";
        switch (jenis) {
            case "Ayam Goreng Telur | Dada":
                sql = "UPDATE " + table_name + " SET jumlah = jumlah-"+jumlah+" WHERE nama= 'Ayam';";
                db.execSQL(sql);
                sql = "UPDATE " + table_name + " SET jumlah = jumlah-"+jumlah+" WHERE nama= 'Telur';";
                db.execSQL(sql);
                break;
            case "Ayam Goreng Telur | Paha":
                sql = "UPDATE " + table_name + " SET jumlah = jumlah-"+jumlah+" WHERE nama= 'Ayam';";
                db.execSQL(sql);
                sql = "UPDATE " + table_name + " SET jumlah = jumlah-"+jumlah+" WHERE nama= 'Telur';";
                db.execSQL(sql);
                break;
            case "Ayam Bakar | Dada":
                sql = "UPDATE " + table_name + " SET jumlah = jumlah-"+jumlah+" WHERE nama= 'Ayam';";
                db.execSQL(sql);
                break;
            case "Ayam Bakar | Paha":
                sql = "UPDATE " + table_name + " SET jumlah = jumlah-"+jumlah+" WHERE nama= 'Ayam';";
                db.execSQL(sql);
                break;
            case "Cah Kangkung":
                sql = "UPDATE " + table_name + " SET jumlah = jumlah-"+jumlah+" WHERE nama= 'Kangkung';";
                db.execSQL(sql);
                break;
            case "Dadar":
                sql = "UPDATE " + table_name + " SET jumlah = jumlah-"+jumlah+" WHERE nama= 'Telur';";
                db.execSQL(sql);
                break;
            case "Kol Goreng":
                sql = "UPDATE " + table_name + " SET jumlah = jumlah-"+jumlah+" WHERE nama= 'Kol';";
                db.execSQL(sql);
                break;
            case "Kulit Krispi":
                sql = "UPDATE " + table_name + " SET jumlah = jumlah-"+jumlah+" WHERE nama= 'Kulit';";
                db.execSQL(sql);
                break;
            case "Jamur Krispi":
                sql = "UPDATE " + table_name + " SET jumlah = jumlah-"+jumlah+" WHERE nama= 'Jamur';";
                db.execSQL(sql);
                break;
            case "Liwet":
                sql = "UPDATE " + table_name + " SET jumlah = jumlah-"+jumlah+" WHERE nama= 'Beras';";
                db.execSQL(sql);
                break;
            case "Nasi Putih":
                sql = "UPDATE " + table_name + " SET jumlah = jumlah-"+jumlah+" WHERE nama= 'Beras';";
                db.execSQL(sql);
                break;
            case "Peda":
                sql = "UPDATE " + table_name + " SET jumlah = jumlah-"+jumlah+" WHERE nama= 'Ayam';";
                db.execSQL(sql);
                break;
            case "Sambal":
                sql = "UPDATE " + table_name + " SET jumlah = jumlah-"+jumlah+" WHERE nama= 'Cabai Merah';";
                db.execSQL(sql);
                break;
            case "Tahu":
                sql = "UPDATE " + table_name + " SET jumlah = jumlah-"+jumlah+" WHERE nama= 'Tahu';";
                db.execSQL(sql);
                break;
            case "Tauge":
                sql = "UPDATE " + table_name + " SET jumlah = jumlah-"+jumlah+" WHERE nama= 'Tauge';";
                db.execSQL(sql);
                break;
            case "Tempe":
                sql = "UPDATE " + table_name + " SET jumlah = jumlah-"+jumlah+" WHERE nama= 'Tempe';";
                db.execSQL(sql);
                break;
            case "Ati Ampela":
                sql = "UPDATE " + table_name + " SET jumlah = jumlah-"+jumlah+" WHERE nama= 'Ati ampela';";
                db.execSQL(sql);
                break;
        }
    }

    //Delete Data
    public void hapusBahan(long id){
        db.delete(table_name, row_id + "=" + id, null);
    }
}

