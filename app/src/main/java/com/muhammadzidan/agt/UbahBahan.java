package com.muhammadzidan.agt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Arrays;

public class UbahBahan extends AppCompatActivity {

    DBHelper helper;
    EditText nama,jumlah;
    Spinner jenis;
    long id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_bahan);
        helper = new DBHelper(this);

        id = getIntent().getLongExtra(DBHelper.row_id, 0);
        nama = (EditText)findViewById(R.id.nama);
        jumlah = (EditText)findViewById(R.id.jumlah);
        jenis = (Spinner)findViewById(R.id.jenis);
        getData();
    }
    private void getData(){
        Cursor cursor = helper.DetailBahan(id);
        if(cursor.moveToFirst()){
            String nama_lama = cursor.getString(cursor.getColumnIndex(DBHelper.row_nama));
            String jumlah_lama = cursor.getString(cursor.getColumnIndex(DBHelper.row_jumlah));
            String jenis_lama = cursor.getString(cursor.getColumnIndex(DBHelper.row_jenis));

            String[] baths = getResources().getStringArray(R.array.jenis_barang);

            jenis.setSelection(Arrays.asList(baths).indexOf(jenis_lama));
            nama.setText(nama_lama);
            jumlah.setText(jumlah_lama);
        }
    }
    public void btn_click(View view){
        ContentValues values = new ContentValues();
        values.put(DBHelper.row_nama, nama.getText().toString());
        values.put(DBHelper.row_jumlah, Integer.parseInt(jumlah.getText().toString()));
        values.put(DBHelper.row_jenis, jenis.getSelectedItem().toString());
        try {
            Toast.makeText(UbahBahan.this, "Sukses ubah data", Toast.LENGTH_SHORT).show();
            helper.ubahBahan(values, id);
        }catch (SQLiteException e){
            Toast.makeText(UbahBahan.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}